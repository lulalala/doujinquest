/**
 * Created by Jerome on 09-02-17.
 */

var Home = {
    maxNameLength : 20 // max length of the name of the player
};

Home.init = function(){
    if(game.device.desktop == false){
        console.log('W : '+window.screen.width+', H : '+window.screen.height);
        if(Math.min(window.screen.width,window.screen.height) < game.width) { // If at least one of the two screen dimensions is smaller for the game, enable asking for device reorientation
            game.scale.scaleMode = Phaser.ScaleManager.RESIZE;
            game.scale.forceOrientation(true,false);
        }
    }
    game.scale.pageAlignHorizontally = true;
    game.add.plugin(Fabrique.Plugins.InputField); // https://github.com/orange-games/phaser-input
    Game.isNewPlayer = Client.isNewPlayer();
};

Home.preload = function(){
    game.load.atlasJSONHash('atlas1', 'assets/sprites/atlas1.png', 'assets/sprites/atlas1.json'); // PNJ, HUD, marker, achievements ...
    game.load.atlasJSONHash('atlas3', 'assets/sprites/atlas3.png', 'assets/sprites/atlas3.json'); // Items, weapons, armors
    game.load.json('db', 'assets/json/db.json');
};

Home.create = function(){
    Game.db = game.cache.getJSON('db');
    if(game.device.desktop == false)
    {
        game.scale.enterIncorrectOrientation.add(Game.displayOrientationScreen, this);
        game.scale.leaveIncorrectOrientation.add(Game.removeOrientationScreen, this);
    }
    if(!Game.isNewPlayer) Home.makeResetScroll();
    Home.displayHomeScroll();
    Home.displayLogo();
    Home.music = game.add.audio('intro');
    Home.music.play();
    document.onkeydown = Home.handleKeyPress;
};

Home.displayHomeScroll = function(){
    if(!Home.scroll) Home.makeHomeScroll();
    if(Home.resetScroll && Home.resetScroll.visible) Home.resetScroll.hideTween.start();
    Home.scroll.visible = true;
    Home.scroll.showTween.start();
};

Home.displayLogo = function(){
    Home.logo = game.add.text(0, 20, 'Doujin Quest');
    Home.logo.anchor.set(0.5,0);
    Home.logo.x = game.width/2;
    Home.logo.hideTween = game.add.tween(Home.logo);
    Home.logo.hideTween.to({alpha: 0}, Phaser.Timer.SECOND*0.2);
};

Home.displayLinks = function(){
    var x = Home.makeLink(300,'About',function(){console.log('about')},true);
    x = Home.makeLink(x+30,'Credits',function(){console.log('credits')},true);
    x = Home.makeLink(x+30,'License',function(){console.log('license')},true);
};

Home.makeLink = function(x,text,callback,hyphen){
    var color = '#b2af9b';
    var style = {font: '18px pixel',fill:color};
    var y = 430;
    var link = game.add.text(x,y,text,style);
    link.inputEnabled = true;
    link.events.onInputOver.add(function(txt){
        txt.addColor('#f4d442',0);
    }, this);
    link.events.onInputOut.add(function(txt){
        txt.addColor(color,0);
    }, this);
    link.events.onInputDown.add(callback, this);
    if(hyphen) {
        var hyphen = game.add.text(link.x+link.width+10,y,' - ',style);
        return hyphen.x;
    }
    return link.x;
};

Home.makeScroll = function(){
    var scroll = game.add.sprite(0,0,'','');
    scroll.x = game.width/2 - scroll.width/2;
    scroll.y = 50;
    scroll.fixedToCamera = true;
    scroll.alpha = 0;
    scroll.visible = false;
    return scroll;
};

Home.setFadeTweens = function(element){
    var speedCoef = 0.2;
    element.showTween = game.add.tween(element);
    element.hideTween = game.add.tween(element);
    element.showTween.to({alpha: 1}, Phaser.Timer.SECOND*speedCoef);
    element.hideTween.to({alpha: 0}, Phaser.Timer.SECOND*speedCoef);
    element.hideTween.onComplete.add(function(){
        element.visible = false;
    },this);
};

Home.makeHomeScroll = function(){
    Game.isNewPlayer = Client.isNewPlayer();
    Home.scroll = Home.makeScroll();
    Home.setFadeTweens(Home.scroll);

    Home.makeTitle(Home.scroll,(Game.isNewPlayer ? 'Create a new character' : 'Load existing character'));

    var buttonY;
    var player;
    if(Game.isNewPlayer){
        player = Home.scroll.addChild(game.add.sprite(0, 110, 'atlas3', 'clotharmor_31'));
        player.alpha = 0.5;
        Home.inputField = Home.scroll.addChild(game.add.inputField(185, 160,{
            width: 300,
            padding: 10,
            backgroundColor: '#d0cdba',
            borderWidth: 2,
            borderColor: '#b2af9b',
            borderRadius: 3,
            font: '18px pixel',
            placeHolder: 'Name your character',
            placeHolderColor: '#b2af9b',
            cursorColor: '#b2af9b',
            max: Home.maxNameLength
        }));
        Home.inputField.x = Home.scroll.width/2 - Home.inputField.width/2;
        Home.inputField.input.useHandCursor = false;
        buttonY = 220;
    }else {
        player = Home.scroll.addChild(game.add.sprite(0, 100, 'atlas3', Client.getArmor()+'_31'));
        var wpn = Client.getWeapon();
        var weapon = player.addChild(game.add.sprite(0, 0, 'atlas3', wpn+'_31'));
        weapon.position.set(Game.db.items[wpn].offsets.x, Game.db.items[wpn].offsets.y);
        var name = player.addChild(game.add.text(0,42, Client.getName(), {
            font: '18px pixel',
        }));
        name.x = Math.floor(12 - (name.width/2));
        Home.makeScrollLink(Home.scroll,'Reset your character',Home.displayResetScroll);
        buttonY = 180;
    }
    player.addChild(game.add.sprite(0,5, 'atlas1','shadow'));
    player.anchor.set(0.25,0.35);
    Home.button = Home.makeButton(Home.scroll,buttonY,'play',Home.startGame);
    if(Game.isNewPlayer) Home.disableButton();
    player.x = Home.button.x - 18;
};

Home.makeTitle = function(scroll,txt){
    var titleY = 65;
    var title = scroll.addChild(game.add.text(0, titleY, txt,{
        font: '18px pixel',
    }));
    title.x = scroll.width/2;
    title.anchor.set(0.5);
};

Home.makeButton = function(scroll,buttonY,frame,callback){
    var text = game.add.text(210, buttonY, 'Submit');
    text.inputEnabled = true;
    text.events.onInputDown.add(callback, this);
    
    var button = scroll.addChild(text);
    button.x = scroll.width/2;
    button.anchor.set(0.5,0);
    button.input.useHandCursor = true;
    return button;
};

Home.makeScrollLink = function(scroll,text,callback){
    var link = scroll.addChild(game.add.text(0,310,text,{
        font: '16px pixel',
    }));
    link.x = scroll.width/2;
    link.anchor.set(0.5);
    link.inputEnabled = true;
    link.events.onInputOver.add(function(txt){
        txt.addColor('#559999',0);
    }, this);
    link.events.onInputOut.add(function(txt){
        txt.clearColors();
    }, this);
    link.events.onInputDown.add(callback, this);
};


Home.displayResetScroll = function(){
    if(!Home.resetScroll) Home.makeResetScroll();
    Home.scroll.hideTween.start();
    Home.resetScroll.visible = true;
    Home.resetScroll.showTween.start();
};

Home.makeResetScroll = function(){
    Home.resetScroll = Home.makeScroll();
    Home.setFadeTweens(Home.resetScroll);
    Home.makeTitle(Home.resetScroll,'Reset your character?');
    var txt = Home.resetScroll.addChild(game.add.text(0,135,'All your progress will be lost. Are you sure?',{
        font: '18px pixel',
    }));
    Home.makeButton(Home.resetScroll,180,'delete',Home.deletePlayer);
    txt.anchor.set(0.5);
    txt.x = Home.resetScroll.width/2;
    Home.makeScrollLink(Home.resetScroll,'Cancel',Home.displayHomeScroll);
};

Home.deletePlayer = function(){
    Client.deletePlayer();
    Home.scroll.destroy();
    Home.scroll = null;
    Home.displayHomeScroll();
};

Home.isNameEmpty = function(){
    return (Home.inputField.text.text.length == 0);
};

Home.startGame = function(){
    var ok = true;
    if(Game.isNewPlayer) {
        if(!Home.isNameEmpty()){
            Client.setName(Home.inputField.text.text);
        }else{
            ok = false;
        }
    }
    if(ok) {
        document.onkeydown = null;
        Home.scroll.hideTween.onComplete.add(function(){
            game.state.start('Game');
            document.querySelector('#web').src = 'https://www.dlsite.com/home/work/=/product_id/RJ284015.html';
            document.querySelector('#web').style.display = 'block';
        },this);
        Home.scroll.hideTween.start();
        Home.logo.hideTween.start();
    }
};

Home.disableButton = function(){
    Home.button.inputEnabled = false;
};

Home.enableButton = function(){
    Home.button.inputEnabled = true;
};

Home.handleKeyPress = function(e){
    e = e || window.event;
    if(e.keyCode == 13) Home.startGame();
};

Home.update = function () {
    if(Home.inputField) {
        Home.inputField.update();
        if (Home.button.inputEnabled) {
            if (Home.isNameEmpty()) Home.disableButton();
        } else {
            if (!Home.isNameEmpty()) Home.enableButton();
        }
    }
};
